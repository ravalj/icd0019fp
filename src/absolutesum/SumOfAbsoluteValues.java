package absolutesum;

import java.util.List;

public class SumOfAbsoluteValues {


    public static int calculateSumOfAbsValues(List<Integer> values) {
        if (values == null) {
            throw new IllegalArgumentException("null is not allowed");
        }
        return values.stream()
                .mapToInt(Math::abs)
                .sum();
    }
}
