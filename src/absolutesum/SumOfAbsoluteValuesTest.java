package absolutesum;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static absolutesum.SumOfAbsoluteValues.calculateSumOfAbsValues;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SumOfAbsoluteValuesTest {

    @Test
    public void calculateSum() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        int sum = calculateSumOfAbsValues(integers);

        assertThat(45, is(sum));
    }

    @Test
    public void calculateSumOfNegativeValues() {
        List<Integer> integers = Arrays.asList(-1, -2, -3, -4, -5, -6, -7, -8, -9);
        int sum = calculateSumOfAbsValues(integers);

        assertThat(45, is(sum));
    }

    @Test
    public void calculateSumOfEmptyList() {
        List<Integer> integers = Collections.emptyList();
        int sum = calculateSumOfAbsValues(integers);

        assertThat(0, is(sum));
    }

    @Test
    public void calculateSumOfOneElement() {
        List<Integer> integers = Arrays.asList(1);
        int sum = calculateSumOfAbsValues(integers);

        assertThat(1, is(sum));
    }

    @Test
    public void calculateSumOfZero() {
        List<Integer> integers = Arrays.asList(0);
        int sum = calculateSumOfAbsValues(integers);

        assertThat(0, is(sum));
    }

    @Test
    public void calculateSumOfMixedValues() {
        List<Integer> integers = Arrays.asList(-1, 3, -44, -12, 25);
        int sum = calculateSumOfAbsValues(integers);

        assertThat(85, is(sum));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullListNotAllowed() {
        calculateSumOfAbsValues(null);
    }
}
