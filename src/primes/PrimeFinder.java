package primes;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PrimeFinder {

    /**
     * Finds primes between start and end.
     * End is not included in the range.
     */
    public static List<Integer> findPrimesInRange(int start, int end) {
        if (start > end) {
            throw new IllegalArgumentException("Start cannot be greater than end");
        }

        Predicate<Integer> isPrime = number -> number > 1 &&
                IntStream.range(2, number).noneMatch(index -> number % index == 0);

        return IntStream.range(start, end)
                .boxed()
                .filter(isPrime)
                .collect(Collectors.toList());
    }
}
