package collector;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static java.util.stream.Collector.Characteristics.IDENTITY_FINISH;
import static java.util.stream.Collector.Characteristics.UNORDERED;

public class HistogramCollector implements Collector<Double, Map<Integer, Integer>, Map<Integer, Integer>> {

    private int bucketSize;

    public HistogramCollector(int bucketSize) {
        this.bucketSize = bucketSize;
    }

    public static HistogramCollector toHistogram(int bucketSize) {
        return new HistogramCollector(bucketSize);
    }

    /**
     * @return a function that creates an empty accumulator
     */
    @Override
    public Supplier<Map<Integer, Integer>> supplier() {
        return HashMap::new;
    }

    /**
     * @return a function which performs the reduction operation.
     * It accepts two arguments. First one being the mutable result container (accumulator) and
     * the second one the stream element that should be folded into the result container.
     */
    @Override
    public BiConsumer<Map<Integer, Integer>, Double> accumulator() {
        System.out.println();
        return (map, value) -> map.put(bucketSize, bucketSize * 2);
    }
    /**
     * When the stream is collected in parallel then the combiner() method is used to return a
     * function which knows how to merge two accumulators.
     */
    @Override
    public BinaryOperator<Map<Integer, Integer>> combiner() {
        // not needed to implement, there's no need to add support for parallel streams
        return (x, y) -> x;
    }

    /**
     * @return a function which performs the final transformation from the intermediate result container
     * to the final result. Often times the accumulator already represents the final result,
     * so the finisher can return the identity function.
     */
    @Override
    public Function<Map<Integer, Integer>, Map<Integer, Integer>> finisher() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return an immutable set of Characteristics which define the behavior of the collector.
     * This is used to check which kind of optimizations can be done during the reduction process.
     * For example, if the set contains CONCURRENT, then the collection process can be performed in parallel.
     */
    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(IDENTITY_FINISH, UNORDERED);
    }
}
