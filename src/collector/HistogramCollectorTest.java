package collector;

import org.junit.Test;

import java.util.*;

import static collector.HistogramCollector.toHistogram;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class HistogramCollectorTest {

    @Test
    public void collectToHistogram() {
        List<Double> numbers = Arrays.asList(1.0, 1.1, 1.4, 1.7, 1.4, 5.4, 9.9);
        Map<Integer, Integer> histogram = numbers.stream().collect(toHistogram(1));
        System.out.println(histogram);

        assertThat(histogram.size(), is(3));
        assertThat(histogram.get(1), is(5));
        assertThat(histogram.get(5), is(1));
        assertThat(histogram.get(9), is(1));
    }

    @Test
    public void collectToHistogramWithBucketSize10() {
        List<Double> numbers = Arrays.asList(1.0, 1.1, 1.4, 1.7, 1.4, 5.4, 9.9, 20.0, 13.4, 3.5, 6.5);
        Map<Integer, Integer> histogram = numbers.stream().collect(toHistogram(10));

        assertThat(histogram.size(), is(3));
    }

    @Test
    public void collectToHistogramWithBucketSize100() {
        List<Double> numbers = Arrays.asList(1.0, 1.1, 1.4, 1.7, 5.4, 9.7, 20.0, 13.4, 3.5, 6.5);
        Map<Integer, Integer> histogram = numbers.stream().collect(toHistogram(100));

        assertThat(histogram.size(), is(1));
    }

    @Test
    public void toHistogramFromEmptyStream() {
        List<Double> numbers = Collections.emptyList();
        Map<Integer, Integer> histogram = numbers.stream().collect(toHistogram(10));

        assertTrue(histogram.isEmpty());
    }

    @Test
    public void toHistogramFromOneElement() {
        List<Double> numbers = Collections.singletonList(-2.0);
        Map<Integer, Integer> histogram = numbers.stream().collect(toHistogram(2));

        assertThat(histogram.size(), is(1));
    }
}
