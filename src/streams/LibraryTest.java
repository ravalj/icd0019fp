package streams;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class LibraryTest {

    private List<Book> books;

    @Before
    public void setUp() {
        Book b1 = new Book("War of the Worlds", "H.G. Wells", 192);
        Book b2 = new Book("2001: A Space Odyssey", "Arthur C. Clarke", 297);
        Book b3 = new Book("The City and the Stars", "Arthur C. Clarke", 255);
        Book b4 = new Book("1984", "George Orwell", 237);
        Book b5 = new Book("Animal Farm", "George Orwell", 144);
        Book b6 = new Book("Lord of the Flies", "William Golding", 184);
        Book b7 = new Book("Alice's Adventures in Wonderland", "Lewis Carroll", 96);

        books = Arrays.asList(b1, b2, b3, b4, b5, b6, b7);
    }

    @Test
    public void findAllAuthors() {
        Library library = new Library(books);

        assertThat(library.findAllAuthors().size(), is(5));
        assertTrue(library.findAllAuthors().contains("H.G. Wells"));
        assertTrue(library.findAllAuthors().contains("Arthur C. Clarke"));
        assertTrue(library.findAllAuthors().contains("George Orwell"));
        assertTrue(library.findAllAuthors().contains("William Golding"));
        assertTrue(library.findAllAuthors().contains("Lewis Carroll"));
    }

    @Test
    public void findAllAuthorsFromEmptyLibrary() {
        Library library = new Library(Collections.emptyList());

        assertThat(library.findAllAuthors().size(), is(0));
    }

    @Test
    public void findAllBookTitlesFromEmptyLibrary() {
        Library library = new Library(Collections.emptyList());

        assertThat(library.findAllBookTitles().size(), is(0));
    }

    @Test
    public void findAllBookTitles() {
        Library library = new Library(books);

        assertThat(library.findAllBookTitles().size(), is(7));
        assertTrue(library.findAllBookTitles().contains("War of the Worlds"));
        assertTrue(library.findAllBookTitles().contains("2001: A Space Odyssey"));
        assertTrue(library.findAllBookTitles().contains("The City and the Stars"));
        assertTrue(library.findAllBookTitles().contains("1984"));
        assertTrue(library.findAllBookTitles().contains("Animal Farm"));
        assertTrue(library.findAllBookTitles().contains("Lord of the Flies"));
        assertTrue(library.findAllBookTitles().contains("Alice's Adventures in Wonderland"));
    }

    @Test
    public void findLongestBookFromEmptyLibrary() {
        Library library = new Library(Collections.emptyList());

        assertFalse(library.findLongestBook().isPresent());
    }

    @Test
    public void findLongestBook() {
        Library library = new Library(books);

        Optional<Book> longestBook = library.findLongestBook();
        assertTrue(longestBook.isPresent());
        longestBook.ifPresent(book -> assertThat(book.getTitle(), is("2001: A Space Odyssey")));
    }

    @Test
    public void findFirstBookByAuthorFromEmptyLibrary() {
        Library library = new Library(Collections.emptyList());

        assertFalse(library.findFirstBookByAuthor("George Orwell").isPresent());
    }

    @Test
    public void findFirstBookByAuthorThatDoesNotExist() {
        Library library = new Library(books);

        assertFalse(library.findFirstBookByAuthor("J.R.R Tolkien").isPresent());
    }

    @Test
    public void findFirstBookByAuthor() {
        Library library = new Library(books);

        Optional<Book> book = library.findFirstBookByAuthor("George Orwell");
        assertTrue(book.isPresent());
        book.ifPresent(b -> assertThat(b.getTitle(), is("1984")));
    }

    @Test
    public void groupBooksByAuthorFromEmptyLibrary() {
        Library library = new Library(Collections.emptyList());

        Map<String, List<Book>> booksByAuthor = library.booksByAuthor();
        assertThat(booksByAuthor.size(), is(0));
    }

    @Test
    public void groupBooksByAuthor() {
        Library library = new Library(books);

        Map<String, List<Book>> booksByAuthor = library.booksByAuthor();
        assertThat(booksByAuthor.size(), is(5));
        assertThat(booksByAuthor.get("George Orwell").size(), is(2));
        assertThat(booksByAuthor.get("H.G. Wells").size(), is(1));
        assertThat(booksByAuthor.get("Arthur C. Clarke").size(), is(2));
        assertThat(booksByAuthor.get("William Golding").size(), is(1));
        assertThat(booksByAuthor.get("Lewis Carroll").size(), is(1));
    }

    @Test
    public void totalNumberOfPagesFromEmptyLibrary() {
        Library library = new Library(Collections.emptyList());

        assertThat(library.totalNumberOfPages(), is(0));
    }

    @Test
    public void totalNumberOfPages() {
        Library library = new Library(books);

        assertThat(library.totalNumberOfPages(), is(1405));
    }

    @Test
    public void partitionBooks() {
        Library library = new Library(books);

        Map<Boolean, List<Book>> booksByLength = library.partitionBooksByLength(150);
        assertThat(booksByLength.get(false).size(), is(2));
        assertThat(booksByLength.get(true).size(), is(5));
    }
}
