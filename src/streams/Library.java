package streams;

import jdk.internal.dynalink.linker.ConversionComparator;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.*;

public class Library {

    private List<Book> books;

    public Library(List<Book> books) {
        this.books = books;
    }

    /**
     * @return list of authors whose books are in the library
     */
    public List<String> findAllAuthors() {
        return books.stream()
                .map(Book::getAuthor)
                .distinct()
                .collect(toList());
    }

    /**
     * @return list of all book titles
     */
    public List<String> findAllBookTitles() {
        return books.stream()
                .map(Book::getTitle)
                .collect(toList());

    }

    /**
     * @return book with the most pages in the library
     */
    public Optional<Book> findLongestBook() {
        return books.stream()
                .max(Comparator.comparing(Book::getNumberOfPages));
    }

    /**
     * @return books grouped by author
     */
    public Map<String, List<Book>> booksByAuthor() {
        return books.stream()
                .collect(groupingBy(Book::getAuthor));
    }

    /**
     * @return first book with the given author
     */
    public Optional<Book> findFirstBookByAuthor(String author) {
        return books.stream()
                .filter(book -> author.equals(book.getAuthor()))
                .findFirst();
    }

    /**
     * @return sum of all pages from all books in the Library
     */
    public int totalNumberOfPages() {
        return books.stream()
                .mapToInt(Book::getNumberOfPages)
                .sum();
    }

    /**
     * Partitions books into two groups by the number of pages.
     */
    public Map<Boolean, List<Book>> partitionBooksByLength(int pages) {
        return books.stream()
                .collect(partitioningBy(book -> book.getNumberOfPages() > pages));
    }

}
