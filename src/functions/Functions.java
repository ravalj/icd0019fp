package functions;

public class Functions {

    public static FunctionMap<Integer, Integer> integerFunctionMap() {
        FunctionMap<Integer, Integer> functionMap = new FunctionMap<>();

        // Implement functions and put them to `functionMap` so that tests in
        // FunctionMapTest would pass

        functionMap.addFunction("abs", Math::abs);
        functionMap.addFunction("square", x -> x * x);
        functionMap.addFunction("negate", x -> -x);
        functionMap.addFunction("increment", x -> x + 1);
        functionMap.addFunction("decrement", x -> x - 1);

        return functionMap;
    }
}
