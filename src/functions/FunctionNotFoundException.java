package functions;

public class FunctionNotFoundException extends RuntimeException {

    public FunctionNotFoundException(String name) {
        super(String.format("Function '%s' was not found", name));
    }
}
