package functions;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Lambdas {

    /**
     * @return tagastab Supplier funktsiooni, mis tagastab alati "Tere maailm" stringi
     */
    public static Supplier<String> helloWorldSupplier() {
        return () -> "Tere maailm";
    }

    /**
     * @return tagastab predikaadi, mis ütleb, kas String on liiga pikk või mitte.
     * String on liiga pikk kui see ületab 20 tähemärki.
     */
    public static Predicate<String> isLengthy() {
        return str -> str.length() > 20;
    }

    /**
     * @return funktsioon, mis oskab öelda, kui pikk etteantud string on
     */
    public static Function<String, Integer> stringLength() {
        return String::length;
    }

    /**
     * @return funktsioon, mis võtab vastu kaks Stringi, liidab need kokku ja tagastab nende kogupikkuse.
     */
    public static BiFunction<String, String, Integer> concatenatedLength() {
        return (str1, str2) -> str1.concat(str2).length();
    }
}
